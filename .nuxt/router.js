import Vue from 'vue'
import Router from 'vue-router'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _05a758b8 = () => interopDefault(import('../pages/login_page.vue' /* webpackChunkName: "pages/login_page" */))
const _387959e2 = () => interopDefault(import('../pages/components/accordion.vue' /* webpackChunkName: "pages/components/accordion" */))
const _77117614 = () => interopDefault(import('../pages/components/alert.vue' /* webpackChunkName: "pages/components/alert" */))
const _29c5b2c5 = () => interopDefault(import('../pages/components/animations.vue' /* webpackChunkName: "pages/components/animations" */))
const _13f1ce54 = () => interopDefault(import('../pages/components/avatars.vue' /* webpackChunkName: "pages/components/avatars" */))
const _3b18e91c = () => interopDefault(import('../pages/components/badge_label.vue' /* webpackChunkName: "pages/components/badge_label" */))
const _89802cb2 = () => interopDefault(import('../pages/components/base.vue' /* webpackChunkName: "pages/components/base" */))
const _dba5b94e = () => interopDefault(import('../pages/components/breadcrumb.vue' /* webpackChunkName: "pages/components/breadcrumb" */))
const _d935afca = () => interopDefault(import('../pages/components/buttons.vue' /* webpackChunkName: "pages/components/buttons" */))
const _0e12477d = () => interopDefault(import('../pages/components/cards.vue' /* webpackChunkName: "pages/components/cards" */))
const _1eafcc8e = () => interopDefault(import('../pages/components/color_palette.vue' /* webpackChunkName: "pages/components/color_palette" */))
const _4ca183f0 = () => interopDefault(import('../pages/components/drop_dropdowns.vue' /* webpackChunkName: "pages/components/drop_dropdowns" */))
const _f38ea43a = () => interopDefault(import('../pages/components/fab_buttons.vue' /* webpackChunkName: "pages/components/fab_buttons" */))
const _718f80d6 = () => interopDefault(import('../pages/components/filters.vue' /* webpackChunkName: "pages/components/filters" */))
const _a16a281e = () => interopDefault(import('../pages/components/footer.vue' /* webpackChunkName: "pages/components/footer" */))
const _34d12b9c = () => interopDefault(import('../pages/components/grid.vue' /* webpackChunkName: "pages/components/grid" */))
const _9917bd06 = () => interopDefault(import('../pages/components/height.vue' /* webpackChunkName: "pages/components/height" */))
const _6c660098 = () => interopDefault(import('../pages/components/icons.vue' /* webpackChunkName: "pages/components/icons" */))
const _58914aa2 = () => interopDefault(import('../pages/components/lists.vue' /* webpackChunkName: "pages/components/lists" */))
const _1a1364df = () => interopDefault(import('../pages/components/masonry.vue' /* webpackChunkName: "pages/components/masonry" */))
const _0009cd28 = () => interopDefault(import('../pages/components/modals_dialogs.vue' /* webpackChunkName: "pages/components/modals_dialogs" */))
const _ab04eefc = () => interopDefault(import('../pages/components/notifications.vue' /* webpackChunkName: "pages/components/notifications" */))
const _f0f8f120 = () => interopDefault(import('../pages/components/pagination.vue' /* webpackChunkName: "pages/components/pagination" */))
const _7b641834 = () => interopDefault(import('../pages/components/progress_spinners.vue' /* webpackChunkName: "pages/components/progress_spinners" */))
const _24dfcefd = () => interopDefault(import('../pages/components/scrollable.vue' /* webpackChunkName: "pages/components/scrollable" */))
const _c6e4c792 = () => interopDefault(import('../pages/components/slider.vue' /* webpackChunkName: "pages/components/slider" */))
const _78141764 = () => interopDefault(import('../pages/components/sortable.vue' /* webpackChunkName: "pages/components/sortable" */))
const _01fdffbb = () => interopDefault(import('../pages/components/tables.vue' /* webpackChunkName: "pages/components/tables" */))
const _26501058 = () => interopDefault(import('../pages/components/tabs.vue' /* webpackChunkName: "pages/components/tabs" */))
const _75881fd2 = () => interopDefault(import('../pages/components/timeline.vue' /* webpackChunkName: "pages/components/timeline" */))
const _67736715 = () => interopDefault(import('../pages/components/toolbar.vue' /* webpackChunkName: "pages/components/toolbar" */))
const _7c426d26 = () => interopDefault(import('../pages/components/tooltips.vue' /* webpackChunkName: "pages/components/tooltips" */))
const _242ba250 = () => interopDefault(import('../pages/components/transitions.vue' /* webpackChunkName: "pages/components/transitions" */))
const _188fa460 = () => interopDefault(import('../pages/components/width.vue' /* webpackChunkName: "pages/components/width" */))
const _7b797ba9 = () => interopDefault(import('../pages/dashboard/v1.vue' /* webpackChunkName: "pages/dashboard/v1" */))
const _7b87932a = () => interopDefault(import('../pages/dashboard/v2.vue' /* webpackChunkName: "pages/dashboard/v2" */))
const _87c11828 = () => interopDefault(import('../pages/forms/dynamic_fields.vue' /* webpackChunkName: "pages/forms/dynamic_fields" */))
const _6641ec2d = () => interopDefault(import('../pages/forms/regular_elements.vue' /* webpackChunkName: "pages/forms/regular_elements" */))
const _3e7b7b2c = () => interopDefault(import('../pages/forms/validation.vue' /* webpackChunkName: "pages/forms/validation" */))
const _3f22349e = () => interopDefault(import('../pages/forms/wizard.vue' /* webpackChunkName: "pages/forms/wizard" */))
const _4d729598 = () => interopDefault(import('../pages/forms/wizard/step1.vue' /* webpackChunkName: "pages/forms/wizard/step1" */))
const _4d566696 = () => interopDefault(import('../pages/forms/wizard/step2.vue' /* webpackChunkName: "pages/forms/wizard/step2" */))
const _4d3a3794 = () => interopDefault(import('../pages/forms/wizard/step3.vue' /* webpackChunkName: "pages/forms/wizard/step3" */))
const _d23a5c68 = () => interopDefault(import('../pages/masterdata/content/index.vue' /* webpackChunkName: "pages/masterdata/content/index" */))
const _495ab780 = () => interopDefault(import('../pages/masterdata/midtrans.vue' /* webpackChunkName: "pages/masterdata/midtrans" */))
const _0c0a0dfc = () => interopDefault(import('../pages/masterdata/service_center/index.vue' /* webpackChunkName: "pages/masterdata/service_center/index" */))
const _5045a15d = () => interopDefault(import('../pages/masterdata/sparepart/index.vue' /* webpackChunkName: "pages/masterdata/sparepart/index" */))
const _2ec7e474 = () => interopDefault(import('../pages/pages/blank.vue' /* webpackChunkName: "pages/pages/blank" */))
const _4810fbb2 = () => interopDefault(import('../pages/pages/blank_header_expanded.vue' /* webpackChunkName: "pages/pages/blank_header_expanded" */))
const _496e62d6 = () => interopDefault(import('../pages/pages/chat.vue' /* webpackChunkName: "pages/pages/chat" */))
const _3714bd68 = () => interopDefault(import('../pages/pages/chat2.vue' /* webpackChunkName: "pages/pages/chat2" */))
const _6a0ed85b = () => interopDefault(import('../pages/pages/contact_list.vue' /* webpackChunkName: "pages/pages/contact_list" */))
const _124b915c = () => interopDefault(import('../pages/pages/contact_list_single.vue' /* webpackChunkName: "pages/pages/contact_list_single" */))
const _60fc3644 = () => interopDefault(import('../pages/pages/gallery.vue' /* webpackChunkName: "pages/pages/gallery" */))
const _5b6a0d96 = () => interopDefault(import('../pages/pages/help_faq.vue' /* webpackChunkName: "pages/pages/help_faq" */))
const _0cc89864 = () => interopDefault(import('../pages/pages/invoices.vue' /* webpackChunkName: "pages/pages/invoices" */))
const _709b7832 = () => interopDefault(import('../pages/pages/invoices/index.vue' /* webpackChunkName: "pages/pages/invoices/index" */))
const _5efe140f = () => interopDefault(import('../pages/pages/invoices/_id.vue' /* webpackChunkName: "pages/pages/invoices/_id" */))
const _bf53c090 = () => interopDefault(import('../pages/pages/issues.vue' /* webpackChunkName: "pages/pages/issues" */))
const _e513452a = () => interopDefault(import('../pages/pages/issues/details.vue' /* webpackChunkName: "pages/pages/issues/details" */))
const _ccb804d4 = () => interopDefault(import('../pages/pages/issues/details/_id.vue' /* webpackChunkName: "pages/pages/issues/details/_id" */))
const _6eac0c45 = () => interopDefault(import('../pages/pages/issues/list.vue' /* webpackChunkName: "pages/pages/issues/list" */))
const _7dabc6a6 = () => interopDefault(import('../pages/pages/mailbox.vue' /* webpackChunkName: "pages/pages/mailbox" */))
const _77538eae = () => interopDefault(import('../pages/pages/mailbox/index.vue' /* webpackChunkName: "pages/pages/mailbox/index" */))
const _7778e4ae = () => interopDefault(import('../pages/pages/mailbox/compose.vue' /* webpackChunkName: "pages/pages/mailbox/compose" */))
const _0fdf2849 = () => interopDefault(import('../pages/pages/mailbox/message/_id.vue' /* webpackChunkName: "pages/pages/mailbox/message/_id" */))
const _1cbf669a = () => interopDefault(import('../pages/pages/notes.vue' /* webpackChunkName: "pages/pages/notes" */))
const _fda775fe = () => interopDefault(import('../pages/pages/poi_listing.vue' /* webpackChunkName: "pages/pages/poi_listing" */))
const _6ad195fc = () => interopDefault(import('../pages/pages/pricing_tables.vue' /* webpackChunkName: "pages/pages/pricing_tables" */))
const _63c04b21 = () => interopDefault(import('../pages/pages/settings.vue' /* webpackChunkName: "pages/pages/settings" */))
const _56bd5eac = () => interopDefault(import('../pages/pages/task_board.vue' /* webpackChunkName: "pages/pages/task_board" */))
const _3fcd7433 = () => interopDefault(import('../pages/pages/user_profile.vue' /* webpackChunkName: "pages/pages/user_profile" */))
const _cca6cafc = () => interopDefault(import('../pages/plugins/ajax.vue' /* webpackChunkName: "pages/plugins/ajax" */))
const _55a7a0c0 = () => interopDefault(import('../pages/plugins/calendar.vue' /* webpackChunkName: "pages/plugins/calendar" */))
const _4a84c092 = () => interopDefault(import('../pages/plugins/charts.vue' /* webpackChunkName: "pages/plugins/charts" */))
const _7aed0be6 = () => interopDefault(import('../pages/plugins/code_editor.vue' /* webpackChunkName: "pages/plugins/code_editor" */))
const _d977976e = () => interopDefault(import('../pages/plugins/data_grid.vue' /* webpackChunkName: "pages/plugins/data_grid" */))
const _e8a3401e = () => interopDefault(import('../pages/plugins/datatables.vue' /* webpackChunkName: "pages/plugins/datatables" */))
const _242933a0 = () => interopDefault(import('../pages/plugins/diff_tool.vue' /* webpackChunkName: "pages/plugins/diff_tool" */))
const _2d65c841 = () => interopDefault(import('../pages/plugins/gantt_chart.vue' /* webpackChunkName: "pages/plugins/gantt_chart" */))
const _5118482a = () => interopDefault(import('../pages/plugins/google_maps.vue' /* webpackChunkName: "pages/plugins/google_maps" */))
const _65c07950 = () => interopDefault(import('../pages/plugins/idle_timeout.vue' /* webpackChunkName: "pages/plugins/idle_timeout" */))
const _04257592 = () => interopDefault(import('../pages/plugins/image_cropper.vue' /* webpackChunkName: "pages/plugins/image_cropper" */))
const _8059e9ea = () => interopDefault(import('../pages/plugins/image_cropper2.vue' /* webpackChunkName: "pages/plugins/image_cropper2" */))
const _40d3b5e5 = () => interopDefault(import('../pages/plugins/push_notifications.vue' /* webpackChunkName: "pages/plugins/push_notifications" */))
const _79ae76ba = () => interopDefault(import('../pages/plugins/tour.vue' /* webpackChunkName: "pages/plugins/tour" */))
const _07472600 = () => interopDefault(import('../pages/plugins/tree.vue' /* webpackChunkName: "pages/plugins/tree" */))
const _b621b7be = () => interopDefault(import('../pages/plugins/vector_maps.vue' /* webpackChunkName: "pages/plugins/vector_maps" */))
const _2bdc8387 = () => interopDefault(import('../pages/plugins/vue_good_table.vue' /* webpackChunkName: "pages/plugins/vue_good_table" */))
const _77cc6c66 = () => interopDefault(import('../pages/transaction/direct.vue' /* webpackChunkName: "pages/transaction/direct" */))
const _5412d0c4 = () => interopDefault(import('../pages/transaction/midtrans.vue' /* webpackChunkName: "pages/transaction/midtrans" */))
const _533b8c9d = () => interopDefault(import('../pages/transaction/order/index.vue' /* webpackChunkName: "pages/transaction/order/index" */))
const _dc7ef926 = () => interopDefault(import('../pages/forms/advanced_elements/checkbox_radio.vue' /* webpackChunkName: "pages/forms/advanced_elements/checkbox_radio" */))
const _52822fd8 = () => interopDefault(import('../pages/forms/advanced_elements/color_picker.vue' /* webpackChunkName: "pages/forms/advanced_elements/color_picker" */))
const _12c1fd41 = () => interopDefault(import('../pages/forms/advanced_elements/date_picker.vue' /* webpackChunkName: "pages/forms/advanced_elements/date_picker" */))
const _2c8c6143 = () => interopDefault(import('../pages/forms/advanced_elements/date_range_picker.vue' /* webpackChunkName: "pages/forms/advanced_elements/date_range_picker" */))
const _8295e090 = () => interopDefault(import('../pages/forms/advanced_elements/inputmask.vue' /* webpackChunkName: "pages/forms/advanced_elements/inputmask" */))
const _b9e39112 = () => interopDefault(import('../pages/forms/advanced_elements/multiselect.vue' /* webpackChunkName: "pages/forms/advanced_elements/multiselect" */))
const _22516891 = () => interopDefault(import('../pages/forms/advanced_elements/range_slider.vue' /* webpackChunkName: "pages/forms/advanced_elements/range_slider" */))
const _925f90ea = () => interopDefault(import('../pages/forms/advanced_elements/rating.vue' /* webpackChunkName: "pages/forms/advanced_elements/rating" */))
const _17fb2338 = () => interopDefault(import('../pages/forms/advanced_elements/select2.vue' /* webpackChunkName: "pages/forms/advanced_elements/select2" */))
const _128c04d0 = () => interopDefault(import('../pages/forms/advanced_elements/switches.vue' /* webpackChunkName: "pages/forms/advanced_elements/switches" */))
const _39d1c0c2 = () => interopDefault(import('../pages/forms/advanced_elements/time_picker.vue' /* webpackChunkName: "pages/forms/advanced_elements/time_picker" */))
const _c2309f0e = () => interopDefault(import('../pages/forms/examples/advertising_evaluation_form.vue' /* webpackChunkName: "pages/forms/examples/advertising_evaluation_form" */))
const _2fbe180d = () => interopDefault(import('../pages/forms/examples/booking_form.vue' /* webpackChunkName: "pages/forms/examples/booking_form" */))
const _6fe3da41 = () => interopDefault(import('../pages/forms/examples/car_rental_form.vue' /* webpackChunkName: "pages/forms/examples/car_rental_form" */))
const _27d4040a = () => interopDefault(import('../pages/forms/examples/checkout_form.vue' /* webpackChunkName: "pages/forms/examples/checkout_form" */))
const _7a2dd9bd = () => interopDefault(import('../pages/forms/examples/contact_forms.vue' /* webpackChunkName: "pages/forms/examples/contact_forms" */))
const _1779a590 = () => interopDefault(import('../pages/forms/examples/job_application_form.vue' /* webpackChunkName: "pages/forms/examples/job_application_form" */))
const _a830a1c0 = () => interopDefault(import('../pages/forms/examples/medical_history_form.vue' /* webpackChunkName: "pages/forms/examples/medical_history_form" */))
const _a600b112 = () => interopDefault(import('../pages/forms/examples/registration_form.vue' /* webpackChunkName: "pages/forms/examples/registration_form" */))
const _6e1bad3b = () => interopDefault(import('../pages/forms/examples/rental_application_form.vue' /* webpackChunkName: "pages/forms/examples/rental_application_form" */))
const _5691e60a = () => interopDefault(import('../pages/forms/examples/transaction_feedback_form.vue' /* webpackChunkName: "pages/forms/examples/transaction_feedback_form" */))
const _16fc6d0a = () => interopDefault(import('../pages/forms/wysiwyg/ckeditor.vue' /* webpackChunkName: "pages/forms/wysiwyg/ckeditor" */))
const _9b9881c0 = () => interopDefault(import('../pages/forms/wysiwyg/quill.vue' /* webpackChunkName: "pages/forms/wysiwyg/quill" */))
const _e5944180 = () => interopDefault(import('../pages/masterdata/account/admin/index.vue' /* webpackChunkName: "pages/masterdata/account/admin/index" */))
const _937ef4ae = () => interopDefault(import('../pages/masterdata/account/technician/index.vue' /* webpackChunkName: "pages/masterdata/account/technician/index" */))
const _f011d28e = () => interopDefault(import('../pages/masterdata/account/users/index.vue' /* webpackChunkName: "pages/masterdata/account/users/index" */))
const _5eb3d523 = () => interopDefault(import('../pages/masterdata/content/image_cropper.vue' /* webpackChunkName: "pages/masterdata/content/image_cropper" */))
const _5a701a86 = () => interopDefault(import('../pages/masterdata/damage/master-damage/index.vue' /* webpackChunkName: "pages/masterdata/damage/master-damage/index" */))
const _626d47f6 = () => interopDefault(import('../pages/masterdata/damage/type-of-damage/index.vue' /* webpackChunkName: "pages/masterdata/damage/type-of-damage/index" */))
const _2dac71bc = () => interopDefault(import('../pages/masterdata/device/device-brand/index.vue' /* webpackChunkName: "pages/masterdata/device/device-brand/index" */))
const _4ac496bb = () => interopDefault(import('../pages/masterdata/device/device-type/index.vue' /* webpackChunkName: "pages/masterdata/device/device-type/index" */))
const _914e6e4a = () => interopDefault(import('../pages/masterdata/account/admin/details/_id.vue' /* webpackChunkName: "pages/masterdata/account/admin/details/_id" */))
const _6dbd3c84 = () => interopDefault(import('../pages/masterdata/account/technician/details/_id.vue' /* webpackChunkName: "pages/masterdata/account/technician/details/_id" */))
const _54e6c194 = () => interopDefault(import('../pages/masterdata/account/users/details/_id.vue' /* webpackChunkName: "pages/masterdata/account/users/details/_id" */))
const _1b88ead0 = () => interopDefault(import('../pages/masterdata/damage/master-damage/details/_id.vue' /* webpackChunkName: "pages/masterdata/damage/master-damage/details/_id" */))
const _788adde0 = () => interopDefault(import('../pages/masterdata/damage/type-of-damage/details/_id.vue' /* webpackChunkName: "pages/masterdata/damage/type-of-damage/details/_id" */))
const _d6c2e352 = () => interopDefault(import('../pages/masterdata/device/device-brand/details/_id.vue' /* webpackChunkName: "pages/masterdata/device/device-brand/details/_id" */))
const _dd3e8fd4 = () => interopDefault(import('../pages/masterdata/device/device-type/details/_id.vue' /* webpackChunkName: "pages/masterdata/device/device-type/details/_id" */))
const _3bdd7faa = () => interopDefault(import('../pages/masterdata/content/details/_id/index.vue' /* webpackChunkName: "pages/masterdata/content/details/_id/index" */))
const _4ea0091d = () => interopDefault(import('../pages/masterdata/service_center/details/_id.vue' /* webpackChunkName: "pages/masterdata/service_center/details/_id" */))
const _2a23c138 = () => interopDefault(import('../pages/masterdata/sparepart/details/_id.vue' /* webpackChunkName: "pages/masterdata/sparepart/details/_id" */))
const _19678710 = () => interopDefault(import('../pages/transaction/order/details/_id.vue' /* webpackChunkName: "pages/transaction/order/details/_id" */))
const _7f592654 = () => interopDefault(import('../pages/masterdata/content/details/_id/content_details.vue' /* webpackChunkName: "pages/masterdata/content/details/_id/content_details" */))
const _38ae568f = () => interopDefault(import('../pages/index.vue' /* webpackChunkName: "pages/index" */))

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: decodeURI('/'),
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/login_page",
    component: _05a758b8,
    name: "login_page"
  }, {
    path: "/components/accordion",
    component: _387959e2,
    name: "components-accordion"
  }, {
    path: "/components/alert",
    component: _77117614,
    name: "components-alert"
  }, {
    path: "/components/animations",
    component: _29c5b2c5,
    name: "components-animations"
  }, {
    path: "/components/avatars",
    component: _13f1ce54,
    name: "components-avatars"
  }, {
    path: "/components/badge_label",
    component: _3b18e91c,
    name: "components-badge_label"
  }, {
    path: "/components/base",
    component: _89802cb2,
    name: "components-base"
  }, {
    path: "/components/breadcrumb",
    component: _dba5b94e,
    name: "components-breadcrumb"
  }, {
    path: "/components/buttons",
    component: _d935afca,
    name: "components-buttons"
  }, {
    path: "/components/cards",
    component: _0e12477d,
    name: "components-cards"
  }, {
    path: "/components/color_palette",
    component: _1eafcc8e,
    name: "components-color_palette"
  }, {
    path: "/components/drop_dropdowns",
    component: _4ca183f0,
    name: "components-drop_dropdowns"
  }, {
    path: "/components/fab_buttons",
    component: _f38ea43a,
    name: "components-fab_buttons"
  }, {
    path: "/components/filters",
    component: _718f80d6,
    name: "components-filters"
  }, {
    path: "/components/footer",
    component: _a16a281e,
    name: "components-footer"
  }, {
    path: "/components/grid",
    component: _34d12b9c,
    name: "components-grid"
  }, {
    path: "/components/height",
    component: _9917bd06,
    name: "components-height"
  }, {
    path: "/components/icons",
    component: _6c660098,
    name: "components-icons"
  }, {
    path: "/components/lists",
    component: _58914aa2,
    name: "components-lists"
  }, {
    path: "/components/masonry",
    component: _1a1364df,
    name: "components-masonry"
  }, {
    path: "/components/modals_dialogs",
    component: _0009cd28,
    name: "components-modals_dialogs"
  }, {
    path: "/components/notifications",
    component: _ab04eefc,
    name: "components-notifications"
  }, {
    path: "/components/pagination",
    component: _f0f8f120,
    name: "components-pagination"
  }, {
    path: "/components/progress_spinners",
    component: _7b641834,
    name: "components-progress_spinners"
  }, {
    path: "/components/scrollable",
    component: _24dfcefd,
    name: "components-scrollable"
  }, {
    path: "/components/slider",
    component: _c6e4c792,
    name: "components-slider"
  }, {
    path: "/components/sortable",
    component: _78141764,
    name: "components-sortable"
  }, {
    path: "/components/tables",
    component: _01fdffbb,
    name: "components-tables"
  }, {
    path: "/components/tabs",
    component: _26501058,
    name: "components-tabs"
  }, {
    path: "/components/timeline",
    component: _75881fd2,
    name: "components-timeline"
  }, {
    path: "/components/toolbar",
    component: _67736715,
    name: "components-toolbar"
  }, {
    path: "/components/tooltips",
    component: _7c426d26,
    name: "components-tooltips"
  }, {
    path: "/components/transitions",
    component: _242ba250,
    name: "components-transitions"
  }, {
    path: "/components/width",
    component: _188fa460,
    name: "components-width"
  }, {
    path: "/dashboard/v1",
    component: _7b797ba9,
    name: "dashboard-v1"
  }, {
    path: "/dashboard/v2",
    component: _7b87932a,
    name: "dashboard-v2"
  }, {
    path: "/forms/dynamic_fields",
    component: _87c11828,
    name: "forms-dynamic_fields"
  }, {
    path: "/forms/regular_elements",
    component: _6641ec2d,
    name: "forms-regular_elements"
  }, {
    path: "/forms/validation",
    component: _3e7b7b2c,
    name: "forms-validation"
  }, {
    path: "/forms/wizard",
    component: _3f22349e,
    name: "forms-wizard",
    children: [{
      path: "step1",
      component: _4d729598,
      name: "forms-wizard-step1"
    }, {
      path: "step2",
      component: _4d566696,
      name: "forms-wizard-step2"
    }, {
      path: "step3",
      component: _4d3a3794,
      name: "forms-wizard-step3"
    }]
  }, {
    path: "/masterdata/content",
    component: _d23a5c68,
    name: "masterdata-content"
  }, {
    path: "/masterdata/midtrans",
    component: _495ab780,
    name: "masterdata-midtrans"
  }, {
    path: "/masterdata/service_center",
    component: _0c0a0dfc,
    name: "masterdata-service_center"
  }, {
    path: "/masterdata/sparepart",
    component: _5045a15d,
    name: "masterdata-sparepart"
  }, {
    path: "/pages/blank",
    component: _2ec7e474,
    name: "pages-blank"
  }, {
    path: "/pages/blank_header_expanded",
    component: _4810fbb2,
    name: "pages-blank_header_expanded"
  }, {
    path: "/pages/chat",
    component: _496e62d6,
    name: "pages-chat"
  }, {
    path: "/pages/chat2",
    component: _3714bd68,
    name: "pages-chat2"
  }, {
    path: "/pages/contact_list",
    component: _6a0ed85b,
    name: "pages-contact_list"
  }, {
    path: "/pages/contact_list_single",
    component: _124b915c,
    name: "pages-contact_list_single"
  }, {
    path: "/pages/gallery",
    component: _60fc3644,
    name: "pages-gallery"
  }, {
    path: "/pages/help_faq",
    component: _5b6a0d96,
    name: "pages-help_faq"
  }, {
    path: "/pages/invoices",
    component: _0cc89864,
    children: [{
      path: "",
      component: _709b7832,
      name: "pages-invoices"
    }, {
      path: ":id",
      component: _5efe140f,
      name: "pages-invoices-id"
    }]
  }, {
    path: "/pages/issues",
    component: _bf53c090,
    name: "pages-issues",
    children: [{
      path: "details",
      component: _e513452a,
      name: "pages-issues-details",
      children: [{
        path: ":id?",
        component: _ccb804d4,
        name: "pages-issues-details-id"
      }]
    }, {
      path: "list",
      component: _6eac0c45,
      name: "pages-issues-list"
    }]
  }, {
    path: "/pages/mailbox",
    component: _7dabc6a6,
    children: [{
      path: "",
      component: _77538eae,
      name: "pages-mailbox"
    }, {
      path: "compose",
      component: _7778e4ae,
      name: "pages-mailbox-compose"
    }, {
      path: "message/:id?",
      component: _0fdf2849,
      name: "pages-mailbox-message-id"
    }]
  }, {
    path: "/pages/notes",
    component: _1cbf669a,
    name: "pages-notes"
  }, {
    path: "/pages/poi_listing",
    component: _fda775fe,
    name: "pages-poi_listing"
  }, {
    path: "/pages/pricing_tables",
    component: _6ad195fc,
    name: "pages-pricing_tables"
  }, {
    path: "/pages/settings",
    component: _63c04b21,
    name: "pages-settings"
  }, {
    path: "/pages/task_board",
    component: _56bd5eac,
    name: "pages-task_board"
  }, {
    path: "/pages/user_profile",
    component: _3fcd7433,
    name: "pages-user_profile"
  }, {
    path: "/plugins/ajax",
    component: _cca6cafc,
    name: "plugins-ajax"
  }, {
    path: "/plugins/calendar",
    component: _55a7a0c0,
    name: "plugins-calendar"
  }, {
    path: "/plugins/charts",
    component: _4a84c092,
    name: "plugins-charts"
  }, {
    path: "/plugins/code_editor",
    component: _7aed0be6,
    name: "plugins-code_editor"
  }, {
    path: "/plugins/data_grid",
    component: _d977976e,
    name: "plugins-data_grid"
  }, {
    path: "/plugins/datatables",
    component: _e8a3401e,
    name: "plugins-datatables"
  }, {
    path: "/plugins/diff_tool",
    component: _242933a0,
    name: "plugins-diff_tool"
  }, {
    path: "/plugins/gantt_chart",
    component: _2d65c841,
    name: "plugins-gantt_chart"
  }, {
    path: "/plugins/google_maps",
    component: _5118482a,
    name: "plugins-google_maps"
  }, {
    path: "/plugins/idle_timeout",
    component: _65c07950,
    name: "plugins-idle_timeout"
  }, {
    path: "/plugins/image_cropper",
    component: _04257592,
    name: "plugins-image_cropper"
  }, {
    path: "/plugins/image_cropper2",
    component: _8059e9ea,
    name: "plugins-image_cropper2"
  }, {
    path: "/plugins/push_notifications",
    component: _40d3b5e5,
    name: "plugins-push_notifications"
  }, {
    path: "/plugins/tour",
    component: _79ae76ba,
    name: "plugins-tour"
  }, {
    path: "/plugins/tree",
    component: _07472600,
    name: "plugins-tree"
  }, {
    path: "/plugins/vector_maps",
    component: _b621b7be,
    name: "plugins-vector_maps"
  }, {
    path: "/plugins/vue_good_table",
    component: _2bdc8387,
    name: "plugins-vue_good_table"
  }, {
    path: "/transaction/direct",
    component: _77cc6c66,
    name: "transaction-direct"
  }, {
    path: "/transaction/midtrans",
    component: _5412d0c4,
    name: "transaction-midtrans"
  }, {
    path: "/transaction/order",
    component: _533b8c9d,
    name: "transaction-order"
  }, {
    path: "/forms/advanced_elements/checkbox_radio",
    component: _dc7ef926,
    name: "forms-advanced_elements-checkbox_radio"
  }, {
    path: "/forms/advanced_elements/color_picker",
    component: _52822fd8,
    name: "forms-advanced_elements-color_picker"
  }, {
    path: "/forms/advanced_elements/date_picker",
    component: _12c1fd41,
    name: "forms-advanced_elements-date_picker"
  }, {
    path: "/forms/advanced_elements/date_range_picker",
    component: _2c8c6143,
    name: "forms-advanced_elements-date_range_picker"
  }, {
    path: "/forms/advanced_elements/inputmask",
    component: _8295e090,
    name: "forms-advanced_elements-inputmask"
  }, {
    path: "/forms/advanced_elements/multiselect",
    component: _b9e39112,
    name: "forms-advanced_elements-multiselect"
  }, {
    path: "/forms/advanced_elements/range_slider",
    component: _22516891,
    name: "forms-advanced_elements-range_slider"
  }, {
    path: "/forms/advanced_elements/rating",
    component: _925f90ea,
    name: "forms-advanced_elements-rating"
  }, {
    path: "/forms/advanced_elements/select2",
    component: _17fb2338,
    name: "forms-advanced_elements-select2"
  }, {
    path: "/forms/advanced_elements/switches",
    component: _128c04d0,
    name: "forms-advanced_elements-switches"
  }, {
    path: "/forms/advanced_elements/time_picker",
    component: _39d1c0c2,
    name: "forms-advanced_elements-time_picker"
  }, {
    path: "/forms/examples/advertising_evaluation_form",
    component: _c2309f0e,
    name: "forms-examples-advertising_evaluation_form"
  }, {
    path: "/forms/examples/booking_form",
    component: _2fbe180d,
    name: "forms-examples-booking_form"
  }, {
    path: "/forms/examples/car_rental_form",
    component: _6fe3da41,
    name: "forms-examples-car_rental_form"
  }, {
    path: "/forms/examples/checkout_form",
    component: _27d4040a,
    name: "forms-examples-checkout_form"
  }, {
    path: "/forms/examples/contact_forms",
    component: _7a2dd9bd,
    name: "forms-examples-contact_forms"
  }, {
    path: "/forms/examples/job_application_form",
    component: _1779a590,
    name: "forms-examples-job_application_form"
  }, {
    path: "/forms/examples/medical_history_form",
    component: _a830a1c0,
    name: "forms-examples-medical_history_form"
  }, {
    path: "/forms/examples/registration_form",
    component: _a600b112,
    name: "forms-examples-registration_form"
  }, {
    path: "/forms/examples/rental_application_form",
    component: _6e1bad3b,
    name: "forms-examples-rental_application_form"
  }, {
    path: "/forms/examples/transaction_feedback_form",
    component: _5691e60a,
    name: "forms-examples-transaction_feedback_form"
  }, {
    path: "/forms/wysiwyg/ckeditor",
    component: _16fc6d0a,
    name: "forms-wysiwyg-ckeditor"
  }, {
    path: "/forms/wysiwyg/quill",
    component: _9b9881c0,
    name: "forms-wysiwyg-quill"
  }, {
    path: "/masterdata/account/admin",
    component: _e5944180,
    name: "masterdata-account-admin"
  }, {
    path: "/masterdata/account/technician",
    component: _937ef4ae,
    name: "masterdata-account-technician"
  }, {
    path: "/masterdata/account/users",
    component: _f011d28e,
    name: "masterdata-account-users"
  }, {
    path: "/masterdata/content/image_cropper",
    component: _5eb3d523,
    name: "masterdata-content-image_cropper"
  }, {
    path: "/masterdata/damage/master-damage",
    component: _5a701a86,
    name: "masterdata-damage-master-damage"
  }, {
    path: "/masterdata/damage/type-of-damage",
    component: _626d47f6,
    name: "masterdata-damage-type-of-damage"
  }, {
    path: "/masterdata/device/device-brand",
    component: _2dac71bc,
    name: "masterdata-device-device-brand"
  }, {
    path: "/masterdata/device/device-type",
    component: _4ac496bb,
    name: "masterdata-device-device-type"
  }, {
    path: "/masterdata/account/admin/details/:id?",
    component: _914e6e4a,
    name: "masterdata-account-admin-details-id"
  }, {
    path: "/masterdata/account/technician/details/:id?",
    component: _6dbd3c84,
    name: "masterdata-account-technician-details-id"
  }, {
    path: "/masterdata/account/users/details/:id?",
    component: _54e6c194,
    name: "masterdata-account-users-details-id"
  }, {
    path: "/masterdata/damage/master-damage/details/:id",
    component: _1b88ead0,
    name: "masterdata-damage-master-damage-details-id"
  }, {
    path: "/masterdata/damage/type-of-damage/details/:id?",
    component: _788adde0,
    name: "masterdata-damage-type-of-damage-details-id"
  }, {
    path: "/masterdata/device/device-brand/details/:id",
    component: _d6c2e352,
    name: "masterdata-device-device-brand-details-id"
  }, {
    path: "/masterdata/device/device-type/details/:id",
    component: _dd3e8fd4,
    name: "masterdata-device-device-type-details-id"
  }, {
    path: "/masterdata/content/details/:id?",
    component: _3bdd7faa,
    name: "masterdata-content-details-id"
  }, {
    path: "/masterdata/service_center/details/:id?",
    component: _4ea0091d,
    name: "masterdata-service_center-details-id"
  }, {
    path: "/masterdata/sparepart/details/:id?",
    component: _2a23c138,
    name: "masterdata-sparepart-details-id"
  }, {
    path: "/transaction/order/details/:id?",
    component: _19678710,
    name: "transaction-order-details-id"
  }, {
    path: "/masterdata/content/details/:id?/content_details",
    component: _7f592654,
    name: "masterdata-content-details-id-content_details"
  }, {
    path: "/",
    component: _38ae568f,
    name: "index"
  }],

  fallback: false
}

export function createRouter () {
  return new Router(routerOptions)
}
