const middleware = {}

middleware['redirect'] = require('../middleware/redirect.js')
middleware['redirect'] = middleware['redirect'].default || middleware['redirect']

middleware['router-auth'] = require('../middleware/router-auth.js')
middleware['router-auth'] = middleware['router-auth'].default || middleware['router-auth']

middleware['server-func'] = require('../middleware/server-func.js')
middleware['server-func'] = middleware['server-func'].default || middleware['server-func']

export default middleware
