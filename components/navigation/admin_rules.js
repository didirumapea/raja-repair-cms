import { scHelpers, scMq } from "~/assets/js/utils";
const { uniqueID } = scHelpers;

export const adminRulesEntries = [
	{
		id: uniqueID(),
		title: "Admin",
		page: "/masterdata/account/admin"
	},
];
