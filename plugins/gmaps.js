import Vue from 'vue'
import * as VueGoogleMaps from 'vue2-google-maps'

Vue.use(VueGoogleMaps, {
	load: {
		key: 'AIzaSyCpa1pnwCPzGr2p69WxMxcQk6HZwXJaZlw',
		libraries: 'places'
	}
});
