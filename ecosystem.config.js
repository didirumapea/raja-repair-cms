module.exports = {
  apps : [
    {
      name: "rr-cms-dev",
      script: "npm",
      args: "run dev"
    },
    {
      name: "rr-cms-prod",
      script: "npm",
      args: "run start"
    },
    {
      name: "rr-cms-staging",
      script: "npm",
      args: "run start"
    }
  ]
}
