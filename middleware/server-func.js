module.exports = function (req, res, next) {
	// req adalah obyek permintaan (request) http Node.js
	// console.log(req.path)
	function test(){
		return 'hello from server func'
	}
	// res adalah obyek respon (response) http Node.js

	// next adalah sebuah function untuk memanggil middleware selanjutnya
	// Jangan lupa untuk memanggil next pada akhir middleware ketika middleware anda bukan sebuah titik pemberhentian (endpoint)!
	next()
}
