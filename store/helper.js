
// let urlCdn = 'http://localhost:3003/';
// STATE AS VARIABLE
export const state = () => ({
  name: 'helper',
	resultDec: null,
	resultEnc: null
})
// ACTIONS AS METHODS
export const actions = { // asyncronous
  async getFeatured1 ({ commit }) {
    return await this.$axios.$get('home/product/type=1')
      .then((response) => {
        return response
      })
      .catch(err => {
        // alert(JSON.stringify(err.response))

        if (err.response === undefined){
          return 'no connection'
          // console.log('No Connection')
        }else{
          console.log(err.response)
        }
        //   return err.response
      })
  },
}
// MUTATION AS LOGIC
export const mutations = { // syncronous
 	encryptor(state, payload){
	  let b64 = this.$CryptoJS.AES.encrypt(payload, process.env.SECRET_KEY).toString();
	  let e64 = this.$CryptoJS.enc.Base64.parse(b64);
	  // let eHex = e64.toString(this.$CryptoJS.enc.Hex);
	  state.resultEnc = e64.toString(this.$CryptoJS.enc.Hex);
  },
	decryptor(state, payload){
		let reb64 = this.$CryptoJS.enc.Hex.parse(payload);
		let bytes = reb64.toString(this.$CryptoJS.enc.Base64);
		let decrypt = this.$CryptoJS.AES.decrypt(bytes, process.env.SECRET_KEY);
		// var plain = decrypt.toString(CryptoJS.enc.Utf8);
		state.resultDec = decrypt.toString(this.$CryptoJS.enc.Utf8);
	},
}
// GETTERS AS DEPLOY RESULT DATA
export const getters = {

}
