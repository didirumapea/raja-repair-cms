import moment from "moment";
// const urlCdn = 'https://cdn.rimember.id/';
let urlCdn =  'https://cdn.rajarepair.id/';
if (process.env.NODE_ENV === 'development'){
	urlCdn = 'https://cdn.rajarepair.id/';
	// urlCdn = 'http://localhost:3011/raja-repair-files/assets/images/';
}
// STATE AS VARIABLE
export const state = () => ({
  pathImgMemberImages: urlCdn + 'assets/images/web-images/member-images/',
  pathImgPromoMerchant: urlCdn + 'assets/images/web-images/promo-merchant/',
  pathImgCategory: urlCdn + 'assets/images/web-images/category/',
  pathImgBanner: urlCdn + 'assets/images/web-images/banner/',
  pathImgContent: urlCdn + 'assets/images/content/',
  pathImgMerchant: urlCdn + 'assets/images/web-images/merchant/',
  d2: null,
  timezone: moment().utcOffset()/60
})
// ACTIONS AS METHODS
export const actions = { // asyncronous
  async getFeatured1 ({ commit }) {
    return await this.$axios.$get('home/product/type=1')
      .then((response) => {
        return response
      })
      .catch(err => {
        // alert(JSON.stringify(err.response))

        if (err.response === undefined){
          return 'no connection'
          // console.log('No Connection')
        }else{
          console.log(err.response)
        }
        //   return err.response
      })
  },
}
// MUTATION AS LOGIC
export const mutations = { // syncronous
  d1(state, payload){
    // console.log(payload)
    state.isTabActive = payload
  },
}
// GETTERS AS DEPLOY RESULT DATA
export const getters = {
  pathImgPromoMerchant: state => {
    return state.pathImgPromoMerchant
  },
  pathImgCategory: state => {
    return state.pathImgCategory
  },
  pathImgBanner: state => {
    return state.pathImgBanner
  },
  pathImgMerchant: state => {
    return state.pathImgMerchant
  },
  pathImgMemberImages: state => {
    return state.pathImgMemberImages
  },
}
