// import config from '~/store/config'
import {encryptAPI, decryptAPI} from '../plugins/global-function'
export const state = () => ({
	listDirectTransaction: [],
	tempImageFile: null,
	dataContent: {},
	fileProgressPercentage: 0,
	statusContent: 'add'
})
// ACTIONS AS METHODS
export const actions = { // asyncronous
	async nuxtServerInit({ commit }, { req }){
		// commit()
		// console.log('server init')
	},
	//region SERVICE CENTER
	async getListServiceCenter ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.$get(`masterdata/service-center/list/page=${payload.page}/limit=${payload.perPage}/column-sort=${payload.sort.field}/sort=${payload.sort.type}`)
			.then((response) => {
				// console.log(response)
				// commit('setlistVoucherNotUsed', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async getListSearchServiceCenter ({ commit }, payload) {
		return await this.$axios.$get(`masterdata/service-center/list/search-global=${payload.searchVal}/page=${payload.page}/limit=${payload.perPage}/column-sort=${payload.sort.field}/sort=${payload.sort.type}`)
			.then((response) => {
				// console.log(response)
				// commit('setlistVoucherNotUsed', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async addServiceCenter ({ commit }, payload) {
		// console.log(payload)
		let _data = {
			name: payload.name,
			address: payload.address,
			city: payload.city,
			phone: payload.phone,
			coordinat: {
				lat: payload.lat,
				lng: payload.lng
			}
		}
		return await this.$axios.post('masterdata/service-center/add', _data)
			.then((response) => {
				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async editServiceCenter ({ commit }, payload) {
		// console.log(payload)
		// showNotification('good', false, 'success')
		let _data = {
			id: payload.id,
			name: payload.mName,
			address: payload.address,
			city: payload.city,
			phone: payload.phone,
			coordinat: {
				lat: payload.lat,
				lng: payload.lng
			}
		}
		return await this.$axios.post('masterdata/service-center/update', _data)
			.then((response) => {
				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async safeDeleteServiceCenter ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.post('masterdata/service-center/safe-delete', {id: payload, delStatus: '1'})
			.then((response) => {
				// console.log(response.data)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async deleteServiceCenter ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.delete('masterdata/service-center/delete/'+payload)
			.then((response) => {
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	//endregion
	//region TECHNICIAN
	async getListTechnician ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.$get(`masterdata/technician/list/page=${payload.page}/limit=${payload.perPage}/column-sort=${payload.sort.field}/sort=${payload.sort.type}`)
			.then((response) => {
				// console.log(response)
				// commit('setlistVoucherNotUsed', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async getListSearchTechnician ({ commit }, payload) {
		return await this.$axios.$get(`masterdata/technician/list/search-global=${payload.searchVal}/page=${payload.page}/limit=${payload.perPage}/column-sort=${payload.sort.field}/sort=${payload.sort.type}`)
			.then((response) => {
				// console.log(response)
				// commit('setlistVoucherNotUsed', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async addTechnician ({ commit }, payload) {
		let techData = {
			nik: payload.nik.replace(/-|_/g, ''),
			name: payload.name,
			email: payload.email,
			password: payload.password,
			phone: payload.phone.replace(/-|_/g, ''),
			address: payload.address,
			service_center_id: payload.service_center_id,
			tech_type: payload.tech_type,
		}
		return await this.$axios.post('masterdata/technician/add', techData)
			.then((response) => {
				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async editTechnician ({ commit }, payload) {
		// showNotification('good', false, 'success')
		let techData = {
			id: payload.id,
			nik: payload.nik.replace(/-|_/g, ''),
			name: payload.name,
			email: payload.email,
			password: payload.password,
			phone: payload.phone.replace(/-|_/g, ''),
			address: payload.address,
			service_center_id: payload.service_center_id,
			tech_type: payload.tech_type,
		}
		return await this.$axios.post('masterdata/technician/update', techData)
			.then((response) => {
				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async safeDeleteTechnician ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.post('masterdata/technician/safe-delete', {id: payload, delStatus: '1'})
			.then((response) => {
				// console.log(response.data)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async deleteTechnician ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.delete('masterdata/technician/delete/'+payload)
			.then((response) => {
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	//endregion
	//region USERS
	async getListUsers ({ commit }, payload) {
		// console.log(payload)
		let encodes = encryptAPI(JSON.stringify(payload.columnFilters))
		// payload.columnFilters = encryptAPI(JSON.stringify(payload.columnFilters))
		return await this.$axios.$get(`masterdata/users/list/page=${payload.page}/limit=${payload.perPage}/column-sort=${payload.sort.field}/sort=${payload.sort.type}/column-filter=${encodes}`)
			.then((response) => {
				// console.log(response)
				// commit('setlistVoucherNotUsed', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async getUsers ({ commit }, payload) {
		payload.columnFilters = encryptAPI(JSON.stringify(payload.columnFilters))
		return await this.$axios.$get(`masterdata/users/list/page=${payload.page}/limit=${payload.perPage}/column-sort=${payload.sort.field}/sort=${payload.sort.type}/column-filter=null`)
			.then((response) => {
				// console.log(response)
				// commit('setlistVoucherNotUsed', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async getListSearchUsers ({ commit }, payload) {
		return await this.$axios.$get(`masterdata/users/list/search-global=${payload.searchVal}/page=${payload.page}/limit=${payload.perPage}/column-sort=${payload.sort.field}/sort=${payload.sort.type}`)
			.then((response) => {
				// console.log(response)
				// commit('setlistVoucherNotUsed', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async addUsers ({ commit }, payload) {
		let techData = {
			nik: payload.nik.replace(/-|_/g, ''),
			name: payload.name,
			email: payload.email,
			password: payload.password,
			phone: payload.phone.replace(/-|_/g, ''),
			address: payload.address,
			service_center_id: payload.service_center_id,
			tech_type: payload.tech_type,
		}
		return await this.$axios.post('masterdata/technician/add', techData)
			.then((response) => {
				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async editUsers ({ commit }, payload) {
		// showNotification('good', false, 'success')
		let techData = {
			id: payload.id,
			nik: payload.nik.replace(/-|_/g, ''),
			name: payload.name,
			email: payload.email,
			password: payload.password,
			phone: payload.phone.replace(/-|_/g, ''),
			address: payload.address,
			service_center_id: payload.service_center_id,
			tech_type: payload.tech_type,
		}
		return await this.$axios.post('masterdata/technician/update', techData)
			.then((response) => {
				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async safeDeleteUsers ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.post('masterdata/users/safe-delete', {id: payload, delStatus: '1'})
			.then((response) => {
				// console.log(response.data)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async deleteUsers ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.delete('masterdata/users/delete/'+payload)
			.then((response) => {
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	//endregion
	//region ADMIN
	async getListAdmin ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.$get(`masterdata/admin/list/page=${payload.page}/limit=${payload.perPage}/column-sort=${payload.sort.field}/sort=${payload.sort.type}`)
			.then((response) => {
				// console.log(response)
				// commit('setlistVoucherNotUsed', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async getListSearchAdmin ({ commit }, payload) {
		return await this.$axios.$get(`masterdata/admin/list/search-global=${payload.searchVal}/page=${payload.page}/limit=${payload.perPage}/column-sort=${payload.sort.field}/sort=${payload.sort.type}`)
			.then((response) => {
				// console.log(response)
				// commit('setlistVoucherNotUsed', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async addAdmin ({ commit }, payload) {
		// console.log(payload)
		payload.phone = payload.phone.replace(/-|_/g, '')
		return await this.$axios.post('masterdata/admin/add', payload)
			.then((response) => {
				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async editAdmin ({ commit }, payload) {
		payload.phone = payload.phone.replace(/-|_/g, '')
		return await this.$axios.post('masterdata/admin/update', payload)
			.then((response) => {
				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async safeDeleteAdmin ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.post('masterdata/admin/safe-delete', {id: payload, delStatus: '1'})
			.then((response) => {
				// console.log(response.data)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async deleteAdmin ({ commit }, payload) {
		return await this.$axios.delete('masterdata/admin/delete/'+payload)
			.then((response) => {
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	//endregion
	//region SPAREPART
	async getListSparepart ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.$get(`masterdata/sparepart/list/page=${payload.page}/limit=${payload.perPage}/column-sort=${payload.sort.field}/sort=${payload.sort.type}`)
			.then((response) => {
				// console.log(response)
				// commit('setlistVoucherNotUsed', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async getListSearchSparepart ({ commit }, payload) {
		return await this.$axios.$get(`masterdata/sparepart/list/search-global=${payload.searchVal}/page=${payload.page}/limit=${payload.perPage}/column-sort=${payload.sort.field}/sort=${payload.sort.type}`)
			.then((response) => {
				// console.log(response)
				// commit('setlistVoucherNotUsed', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async addSparepart ({ commit }, payload) {
		let data = {
			name: payload.name,
			mst_damage_type_id: payload.mst_damage_type_id,
			detail: payload.detail,
			img_url: payload.img_url,
			price: payload.price.replace(/\D/g,''),
			devices_type_id: payload.devices_type_id,
		}
		return await this.$axios.post('masterdata/sparepart/add', data)
			.then((response) => {
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async editSparepart ({ commit }, payload) {
		let data = {
			id: payload.id,
			name: payload.name,
			mst_damage_type_id: payload.mst_damage_type_id,
			detail: payload.detail,
			img_url: payload.img_url,
			price: payload.price.replace(/\D/g,''),
			devices_type_id: payload.devices_type_id,
		}
		return await this.$axios.post('masterdata/sparepart/update', data)
			.then((response) => {
				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async safeDeleteSparepart ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.post('masterdata/sparepart/safe-delete', {id: payload, delStatus: '1'})
			.then((response) => {
				// console.log(response.data)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async deleteSparepart ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.delete('masterdata/sparepart/delete/'+payload)
			.then((response) => {
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	//endregion
	//region TYPE OF DAMAGE
	async getListTypeOfDamage ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.$get(`masterdata/damage-type/list/page=${payload.page}/limit=${payload.perPage}/column-sort=${payload.sort.field}/sort=${payload.sort.type}`)
			.then((response) => {
				// console.log(response)
				// commit('setlistVoucherNotUsed', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async getListSearchTypeOfDamage ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.$get(`masterdata/damage-type/list/search-global=${payload.searchVal}/page=${payload.page}/limit=${payload.perPage}/column-sort=${payload.sort.field}/sort=${payload.sort.type}`)
			.then((response) => {
				// console.log(response)
				// commit('setlistVoucherNotUsed', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async addTypeOfDamage ({ commit }, payload) {
		let data = {
			type_id: payload.id_type_of_damage,
			desc: payload.desc,
			device_type_id: payload.id_device_type,
			price: payload.price.replace(/\D/g,'')
		}
		return await this.$axios.post('masterdata/damage-type/add', data)
			.then((response) => {
				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async editTypeOfDamage ({ commit }, payload) {
		// console.log(payload)
		let data = {
			"id": payload.id,
			"mst_damage_type_id": payload.id_type_of_damage,
			"device_type_id": payload.id_device_type,
			"name": payload.desc,
			"price": payload.price.replace(/\D/g,'')
		}
		return await this.$axios.post('masterdata/damage-type/update', data)
			.then((response) => {
				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async safeDeleteTypeOfDamage ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.post('masterdata/damage-type/safe-delete', {id: payload, delStatus: '1'})
			.then((response) => {
				// console.log(response.data)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async deleteTypeOfDamage ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.delete('masterdata/sparepart/delete/'+payload)
			.then((response) => {
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	//endregion
	//region DEVICE
	async getListDevice ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.$get(`masterdata/device/list/page=${payload.page}/limit=${payload.perPage}/column-sort=${payload.sort.field}/sort=${payload.sort.type}`)
			.then((response) => {
				// console.log(response)
				// commit('setlistVoucherNotUsed', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	// DEVICE TYPE
	async getListDeviceType ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.$get(`masterdata/device/type/list/page=${payload.page}/limit=${payload.perPage}/column-sort=${payload.sort.field}/sort=${payload.sort.type}`)
			.then((response) => {
				// console.log(response)
				// commit('setlistVoucherNotUsed', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async addDeviceType ({ commit }, payload) {
		// console.log(payload)
		let data = {
			"name": payload.name,
			"devices_brand_id": payload.devices_brand_id,
			"resolution": payload.resolution,
			"memory": payload.memory,
			"processor": payload.processor
		}
		return await this.$axios.post('masterdata/device/type/add', data)
			.then((response) => {
				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async editDeviceType ({ commit }, payload) {
		let data = {
			"id": payload.id,
			"name": payload.name,
			"devices_brand_id": payload.devices_brand_id,
			"resolution": payload.resolution,
			"memory": payload.memory,
			"processor": payload.processor
		}
		return await this.$axios.post('masterdata/device/type/update', data)
			.then((response) => {
				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async getListDeviceTypeByBrandId ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.$get(`masterdata/device/type/list/brand_id=${payload}/page=1/limit=100/column-sort=id/sort=desc}`)
			.then((response) => {
				// console.log(response)
				// commit('setlistVoucherNotUsed', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async getListSearchDeviceType ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.$get(`masterdata/device/type/list/search-global=${payload.searchVal}/page=${payload.page}/limit=${payload.perPage}/column-sort=${payload.sort.field}/sort=${payload.sort.type}`)
			.then((response) => {
				// console.log(response)
				// commit('setlistVoucherNotUsed', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async safeDeleteDeviceType ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.post('masterdata/device/type/safe-delete', {id: payload, delStatus: '1'})
			.then((response) => {
				// console.log(response.data)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async deleteDeviceType ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.delete('masterdata/device/type/delete/'+payload)
			.then((response) => {
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	// DEVICE BRAND
	async getListDeviceBrand ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.$get(`masterdata/device/brand/list/page=${payload.page}/limit=${payload.perPage}/column-sort=${payload.sort.field}/sort=${payload.sort.type}`)
			.then((response) => {
				// console.log(response)
				// commit('setlistVoucherNotUsed', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async addDeviceBrand ({ commit }, payload) {
		// console.log(payload.price.replace(/\D/g,''))
		let data = {
			name: payload.name,
			img_url: payload.img_url
		}
		return await this.$axios.post('masterdata/device/brand/add', data)
			.then((response) => {
				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async editDeviceBrand ({ commit }, payload) {
		let data = {
			"id": payload.id,
			"name": payload.name,
			"img_url": payload.img_url
		}
		return await this.$axios.post('masterdata/device/brand/update', data)
			.then((response) => {
				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async getListSearchDeviceBrand ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.$get(`masterdata/device/brand/list/search-global=${payload.searchVal}/page=${payload.page}/limit=${payload.perPage}/column-sort=${payload.sort.field}/sort=${payload.sort.type}`)
			.then((response) => {
				// console.log(response)
				// commit('setlistVoucherNotUsed', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async safeDeleteDeviceBrand ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.post('masterdata/sparepart/safe-delete', {id: payload, delStatus: '1'})
			.then((response) => {
				// console.log(response.data)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async deleteDeviceBrand ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.delete('masterdata/device/brand/delete/'+payload)
			.then((response) => {
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	//endregion
	//region CONTENT
	async getListContent ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.$get(`masterdata/content/list/page=${payload.page}/limit=${payload.perPage}/column-sort=${payload.sort.field}/sort=${payload.sort.type}`)
			.then((response) => {
				// console.log(response)
				// commit('setlistVoucherNotUsed', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async getListSearchContent ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.$get(`masterdata/content/list/search-global=${payload.searchVal}/page=${payload.page}/limit=${payload.perPage}/column-sort=${payload.sort.field}/sort=${payload.sort.type}`)
			.then((response) => {
				// console.log(response)
				// commit('setlistVoucherNotUsed', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async addContent ({ commit }, payload) {
		let formData = new FormData()
		console.log(payload.file.name)
		formData.append('title', payload.title)
		formData.append('content', payload.content)
		formData.append('type', payload.type)
		formData.append('images', payload.file)
		return await this.$axios.post('masterdata/content/add', formData)
			.then((response) => {
				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async editContent ({ commit }, payload) {
		let formData = new FormData()
		formData.append('id', payload.id)
		formData.append('title', payload.title)
		formData.append('content', payload.content)
		formData.append('filename', payload.image)
		formData.append('type', payload.type)
		formData.append('images', payload.file)
		return await this.$axios.post('masterdata/content/update', formData)
			.then((response) => {
				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async safeDeleteContent ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.post('masterdata/content/safe-delete', {id: payload, delStatus: '1'})
			.then((response) => {
				// console.log(response.data)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async deleteContent ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.delete('masterdata/sparepart/delete/'+payload)
			.then((response) => {
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async uploadFiles ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.post(`cms/upload-file/single-file`, payload)
			.then((response) => {
				// console.log(response)
				// commit('setListOrderUser', response.data)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
					return {
						success: false,
						message: err,
					}
				}
			})
	},
	//endregion
	//region MASTER TYPE OF DAMAGE
	async getListMstTypeOfDamage ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.$get(`masterdata/damage-type/list/type-of-damage/page=${payload.page}/limit=${payload.perPage}/column-sort=${payload.sort.field}/sort=${payload.sort.type}`)
			.then((response) => {
				// console.log(response)
				// commit('setlistVoucherNotUsed', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async addMstTypeOfDamage ({ commit }, payload) {
		let data = {
			name: payload.name,
			service_type: payload.service_type,
		}
		return await this.$axios.post('masterdata/damage-type/add-type-of', data)
			.then((response) => {
				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async editMstTypeOfDamage ({ commit }, payload) {
		let data = {
			"id": payload.id,
			"name": payload.name,
			"service_type": payload.service_type
		}
		return await this.$axios.post('masterdata/damage-type/update-type-of', data)
			.then((response) => {
				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async getListSearchMstTypeOfDamage ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.$get(`masterdata/damage-type/list/type-of-damage/search-global=${payload.searchVal}/page=${payload.page}/limit=${payload.perPage}/column-sort=${payload.sort.field}/sort=${payload.sort.type}`)
			.then((response) => {
				// console.log(response)
				// commit('setlistVoucherNotUsed', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async safeDeleteMstTypeOfDamage ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.post('masterdata/damage-type/type-of-damage/safe-delete', {id: payload, delStatus: '1'})
			.then((response) => {
				// console.log(response.data)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	//endregion
}

// MUTATION AS LOGIC
export const mutations = { // syncronous
	updateStatusContent(state, payload){
		state.statusContent = payload
	},
	setTempImageFile(state, payload){
		state.tempImageFile = payload
		state.dataContent.image_url = payload.name
		// console.log(state.tempImageFile)
	},
	clearTempImageFile(state){
		state.tempImageFile = null
		// console.log(state.tempImageFile)
	},
	setlistVoucherNotUsed(state, payload){
		// console.log(payload)
		state.listVoucherNotUsed = payload
		// this.$cookies.get('banner-top', payload)
	},
	setlistVoucherUsed(state, payload){
		// console.log(payload)
		state.listVoucherUsed = payload
		// this.$cookies.get('banner-top', payload)
	},
	setlistVoucherNotPay(state, payload){
		// console.log(payload)
		state.listVoucherNotPay = payload
		// this.$cookies.get('banner-top', payload)
	},
	useVoucher (state, id) {
		// console.log(state.listVoucherNotUsed.data, id)
		state.listVoucherNotUsed.data = state.listVoucherNotUsed.data.filter(item => item.id !== id)
	},
	voucherTimeExpired (state, id) {
		// console.log(state.listVoucherNotUsed.data, id)
		state.listVoucherNotPay.data = state.listVoucherNotPay.data.filter(item => item.id !== id)
	},
	// SAMPLE
	increment (state, payload) {
		state.count+=payload
	},
	add (state, text) {
		state.list.push({
			text: text,
			done: false
		})
	},
	remove (state, { todo }) {
		state.list.splice(state.list.indexOf(todo), 1)
	},
	toggle (state, todo) {
		todo.done = !todo.done
	}
}
// GETTERS AS DEPLOY RESULT DATA
export const getters = {
	listCategory: state => {
		return state.listCategory
	},

}
